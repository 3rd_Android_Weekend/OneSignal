package com.kshrd.onesignal;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.onesignal.OneSignal;

import org.json.JSONObject;

import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences sharedPreferences = getSharedPreferences("SETTING", MODE_PRIVATE);
        String color = sharedPreferences.getString("color", "#ff0000");

        TextView tvColor = (TextView) findViewById(R.id.tvColor);
        tvColor.setTextColor(Color.parseColor(color));

        Log.e("ooooo", OneSignal.getPermissionSubscriptionState().getSubscriptionStatus().getUserId());

        OneSignal.getTags(new OneSignal.GetTagsHandler() {
            @Override
            public void tagsAvailable(JSONObject tags) {
                Log.e("ooooo", tags.toString());
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_item, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.it_news:
                OneSignal.sendTag("IT-News", "true");
                break;
            case R.id.sport_news:
                OneSignal.sendTag("Sport-News", "true");
                break;
            case R.id.clear:
                OneSignal.deleteTags(Arrays.asList("IT-News", "Sport-News"));
                break;
            case R.id.subscribe:
                OneSignal.setSubscription(true);
                break;
            case R.id.unsubscribe:
                OneSignal.setSubscription(false);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
