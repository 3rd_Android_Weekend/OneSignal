package com.kshrd.onesignal;

import android.content.SharedPreferences;
import android.util.Log;

import com.onesignal.NotificationExtenderService;
import com.onesignal.OSNotificationReceivedResult;

import org.json.JSONObject;

/**
 * Created by pirang on 8/5/17.
 */

public class OneSignalExtender extends NotificationExtenderService {
    @Override
    protected boolean onNotificationProcessing(OSNotificationReceivedResult receivedResult) {
        // Read properties from result.
        JSONObject object = receivedResult.payload.additionalData;
        if (object != null){
            String color = object.optString("color", null);
            if (color != null){
                // Use the color value depends on your requirement
                SharedPreferences sharedPreferences = getSharedPreferences("SETTING", MODE_PRIVATE);
                sharedPreferences.edit().putString("color", color).apply();

                Log.e("ooooo", color);
                return true;
            }
        }

        // Return true to stop the notification from displaying.
        return false;
    }
}